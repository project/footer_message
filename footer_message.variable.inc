<?php

/**
 * @file
 * Set variable info.
 */

/**
 * Implements hook_variable_info().
 */
function footer_message_variable_info($options) {
  $variables['footer_message_msg'] = array(
    'type' => 'string',
    'title' => t('Site footer', array(), $options),
    'default' => 'Drupal',
    'description' => t('The footer of this website.', array(), $options),
    'group' => 'site_information',
    'required' => FALSE,
  );

  return $variables;
}
